'''

Si vuole costruire una classe Azienda utile affinchè si possa inserire in essa la lista di tutte le certificazioni
che quest'ultima possiede, inoltre al suo interno verrà creata una lista contenente tutte le aziende che quest'ultima
risulta avere dei rapporti di subappalto, cosicchè ogni volta che fuori esce un nuovo bando, si può controllare se
l'azienda che vuole partecipare al bando ha gia collaborazioni con aziende amiche che posseggono le certificazioni
che possono essere utili al fine di vincere il bando
'''

# Libreria utile per la lettura di un file CSV

from ClassiUtilita.GestoreCertificazioni import GestoreCertificazioni as GC
from ClassiUtilita.Classificatore import Classificatore as Classif
from GaraDAppalto.GaraAppalto import GaraAppalto


class Azienda:


    # Principali caratteristiche dell'azienda
    # nome = ""
    # specializzazione = ""

    # Dizionario in cui andremo a definire le varie certificazioni, il dizionario ha come indice il nome della categoria
    # certificazioni = {}

    # Aziende che possono essere utilizzate per subappalto
    # aziendeSubappalti = {}


    # Costruttore
    def __init__(self, nome, specializzazione):
        self.nome = nome
        self.specializzazione = specializzazione
        self.certificazioni = {}
        self.aziendeSubappalti = {}
        self.gareAppaltoVinte = []
        self.gareAppaltoPerse = []

    # Metodo per l'inserimento di una nuova certificazione
    def inserisciCertificazioni(self, tipo, classifica):

        # Controllo se la certificazione che si vuole inserire risulta essere esistente
        if GC.checkCertificazione(tipo) != -1:
            # Si aggiunge la certificazione al dizionario
            self.certificazioni.update({tipo: {"Descrizione": GC.getDescrizione(tipo),"Classifica": classifica}})
        else:
            # Si specifica che non si può inserire la certificazione in quanto non esiste
            print("Errore Inserimento Certificazione " + tipo + ", questa certificazione non esiste.")




    # Restituisce tutte le certificazioni dell'azienda
    def getCertificazioni(self):
        return self.certificazioni


    # Metodo per l'inserimento delle aziende subappaltanti
    def addAzienda(self, azienda):
        # Inizialmente si estrapolano le certificazioni dell'azienda
        certificazioniEst = azienda.getCertificazioni()

        # Poi per ogni certificazione, si controlla se gia esistono aziende con queste certificazioni, in questo caso
        # si aggiunge anche quest'ultima, altrimenti si crea una nuovo indice all'interno del dizionario, in cui si
        # aggiunge l'azienda
        for key in certificazioniEst:
            if key in self.aziendeSubappalti:
                self.aziendeSubappalti[key].append(azienda)
            else:
                self.aziendeSubappalti.update({key:[azienda]})


    # Si restituiscono i nomi delle aziende che hanno la certificazione richiesta
    def getAziendeSubCertificazione(self,tipo):
        if GC.checkCertificazione(tipo) != -1:
            if tipo in self.aziendeSubappalti:
                return self.aziendeSubappalti[tipo]
            else:
                return 0;
        else:
            print("Errore digitazione Certificazione")




    # Questo metodo è utilizzato per poter capire se si può partecipare ad una gara d'appalto, controllando se si
    # posseggono tutte le certificazioni richieste, soprattutto quella prevalente.

    def checkPartecipazione(self,garaAppalto):

        listCertif = garaAppalto.getCertificazioniRichieste()
        for each in listCertif.keys():
            print(each)

            # Le certificazioni prevalenti sono obbligatorie per  l'azienda che vorrebbe partecipare alla gara d'appalto
            # La mancanza di quest'ultima sancisce l'esclusione dalla gara d'appalto

            if listCertif[each]["Tipo"] == "Prevalente":
                if each in self.certificazioni.keys():
                    if self.certificazioni[each]["Classifica"] >= listCertif[each]["Classifica"]:
                        print("Ok hai la classe prevalente puoi quindi partecipare al bando")
                    else:
                        print("Non pui partecipare in quanto la classifica per la classe prevalente non supera"
                              " quella richiesta")
                else:
                    print("Non hai la classe prevalente, non puoi partecipare")
                    break

            # Nel caso di certificazioni scorporabili si possono subappaltare ad altre aziende, inoltre si esegue
            # un ulteriore controllo per verificare se la certificazione risulta essere obbligatoria o meno

            elif listCertif[each]["Tipo"] == "Scorporabile":

                if each in self.certificazioni.keys():
                    if self.certificazioni[each]["Classifica"] >= listCertif[each]["Classifica"]:
                        print("Puoi anche non subappaltare la categoria " + each + " in quanto ne hai il possesso")
                        break

                if  self.getAziendeSubCertificazione(each) != None:

                    if self.getAziendeSubCertificazione(each) != 0:
                        aziende = self.getAziendeSubCertificazione(each)
                        for x in range(0,len(self.getAziendeSubCertificazione(each))):
                            if aziende[x].getCertificazioni()[each]["Classifica"] >= listCertif[each]["Classifica"]:
                                print("Puoi subappaltare a " + aziende[x].nome)

                    else:
                        if GC.getObbligatorieta(each) == 1:
                            print("Devi subappaltare in quanto è una qualificazione obbligatoria")
                        else:
                            print("Puoi anche non subappaltare in quanto non è una qualificazione obbligatoria")


            print("************")


    def aggiungiGaraVinta(self,garaAppalto):

        self.gareAppaltoVinte.append(garaAppalto)

    def aggiungiGaraPersa(self,garaAppalto):

        self.gareAppaltoPerse.append(garaAppalto)

    def probabilitaVincita(self,garaAppalto):

        Classif.gareAppaltoSimili(garaAppalto,self.gareAppaltoVinte,self.gareAppaltoPerse)









if __name__ == '__main__':

    az = Azienda("Savoiardi","Azzuppamento Latte")
    la = Azienda("Balocchi", "Fatevi e fatt vuostr")
    po = Azienda("YMA", "Il biscuttiello")


    az.inserisciCertificazioni("OS 32",5)
    az.inserisciCertificazioni("OG 1", 6)
    az.inserisciCertificazioni("OG 9", 5)

    po.inserisciCertificazioni("OG 1", 6)


    # az.inserisciCertificazioni("OG 45", "V")



    la.addAzienda(az)
    la.addAzienda(po)

    la.inserisciCertificazioni("OS 32",7)
    # la.getAziendeSubCertificazione("OG 1")


    gara = GaraAppalto("Ricostruzione Scuola Primaria Arischia","Si deve ristrutturare la scuola primaria di arischia", "45214210-5")
    gara1 = GaraAppalto("Ricostruzione Scuola Primaria Ischia", "Si deve ristrutturare la scuola primaria di arischia",
                       "45214210-5")
    gara2 = GaraAppalto("Ricostruzione Scuola Primaria Milano", "Si deve ristrutturare la scuola primaria di arischia",
                       "45214210-5")
    gara3 = GaraAppalto("Ricostruzione Scuola Primaria Genova", "Si deve ristrutturare la scuola primaria di arischia",
                       "45214210-5")
    gara4 = GaraAppalto("Ricostruzione Scuola Primaria Pollio", "Si deve ristrutturare la scuola primaria di arischia",
                        "45214210-5")

    gara.inserisciCertif("OS 32", 3, 884521, "Prevalente")
    gara.inserisciCertif("OG 1", 3, 515651, "Scorporabile")
    gara.inserisciCertif("OS 16", 2, 51612, "Scorporabile")
    gara.inserisciCertif("OG 6", 1, 1516521, "Scorporabile")


    gara1.inserisciCertif("OS 2-A", 3, 546551, "Prevalente")
    gara1.inserisciCertif("OG 3", 3, 156158, "Scorporabile")
    gara1.inserisciCertif("OS 20-A", 2, 60022, "Scorporabile")
    gara1.inserisciCertif("OG 7", 1, 5128222, "Scorporabile")

    gara2.inserisciCertif("OS 2-A", 3, 774166, "Prevalente")
    gara2.inserisciCertif("OS 32", 3, 604843, "Scorporabile")
    gara2.inserisciCertif("OS 20-A", 2, 841321, "Scorporabile")
    gara2.inserisciCertif("OG 7", 1, 119156, "Scorporabile")

    gara4.inserisciCertif("OS 2-A", 3, 774166, "Prevalente")
    gara4.inserisciCertif("OS 32", 3, 604843, "Scorporabile")
    gara4.inserisciCertif("OS 20-A", 2, 400000, "Scorporabile")
    gara4.inserisciCertif("OG 7", 1, 245000, "Scorporabile")

    gara3.inserisciCertif("OS 32", 3, 165165, "Prevalente")
    gara3.inserisciCertif("OG 1", 3, 8541651, "Scorporabile")
    gara3.inserisciCertif("OS 20-A", 2, 516512, "Scorporabile")
    gara3.inserisciCertif("OG 8", 1, 4161564, "Scorporabile")

    la.checkPartecipazione(gara)

    la.aggiungiGaraVinta(gara)
    la.aggiungiGaraVinta(gara1)
    la.aggiungiGaraPersa(gara2)
    la.aggiungiGaraPersa(gara4)

    la.probabilitaVincita(gara3)


    # print(az.getCertificazioni().keys())





