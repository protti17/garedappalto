'''

Questa classe è stata creata per poter gestire le gare d'appalto. Essa permette di inserire e gestire tutte le varie
certificazioni richieste per quest'ultimo.

'''


from ClassiUtilita.GestoreCertificazioni import GestoreCertificazioni as GC

class GaraAppalto:

    # Titolo e descrizione della gara d'appalto
    titolo = ""
    breveDescr = ""

    # Codice univoco per poter identificare una gara d'appalto
    cpv = ""
    # Dizionario con le certificazioni richieste dalla gara d'appalto
    # certifRichieste = {}

    # Costrutture
    def __init__(self,titolo,breveDescr, cpv = ""):
        self.titolo = titolo
        self.breveDescr = breveDescr
        self.cpv = cpv
        self.certifRichieste = {}

    # Metodo per l'inserimenti delle certificazioni per le gare d'appalto
    def inserisciCertif(self,certificazione,classifica,importo,tipoPrevSco):

        # Controllo se la certificazione che si vuole inserire risulta essere esistente
        if GC.checkCertificazione(certificazione) != -1:
            # Si aggiunge la certificazione al dizionario
            self.certifRichieste.update({certificazione: {"Descrizione": GC.getDescrizione(certificazione),
                                                         "Classifica": classifica,"ImportoClass":importo,"Tipo":tipoPrevSco}})
        else:
            # Si specifica che non si può inserire la certificazione in quanto non esiste
            print("Errore Inserimento Certificazione " + certificazione + ", questa certificazione non esiste.")


    def getCertificazioniRichieste(self):
        return self.certifRichieste



if __name__ == '__main__':
    gara = GaraAppalto("Ricostruzione Scuola Primaria Arischia",
                       "Si deve ristrutturare la scuola primaria di arischia","45214210-5")

    gara.inserisciCertif("OS 32",3,774166,"Prevalente")
    gara.inserisciCertif("OG 1", 3, 604843, "Scorporabile")
    gara.inserisciCertif("OG 11", 2, 400000, "Scorporabile")
    gara.inserisciCertif("OG 6", 1, 245000, "Scorporabile")
    cert = gara.getCertificazioniRichieste()

    print(GC.checkCertificazione("OG 11"))



