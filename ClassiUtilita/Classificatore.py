import math

'''
    Questa classe è utilizzata per poter classificare se una nuova gara d'appalto si avvicina di più ad un tipo di
    gara vinto o perso.
    Il metodo utilizzato consiste in un tipo di kNN, in quanto inizialmente vengono prese le gare d'appalto vinte 
    e perse, ed in base a ciò, si fornisce un indice di similarità utile per poter prendere delle gare che risultano 
    essere simili a quest'ultima, in base alle categorie SOA richieste.
    Quindi se le gare d'appalto perse, con le categorie SOA richieste, sono di più di quelle vinte, allora 
    è molto probabile che questa nuova gara possa essere persa.
    Nel caso in cui il numero di gare sono uguali, ossia ci sono lo stesso numero di gare vinte e gare perse, 
    in questo caso si calcola la distanza euclidea fra i vari importi delle caratteristiche, e, la gara che
    risulta essere più lontana fra le altre, viene esclusa dal conteggio delle gare vinte e perse, e quindi 
    verrà calcolata se la gara potrebbe essere vinta o persa
'''


class Classificatore:

    @staticmethod
    def gareAppaltoSimili(garaAppalto, gareAppaltoVinte, gareAppaltoPerse):

        # Si indica il grado di similarità che si vuole fra le gare d'appalto
        indiceSimilarita = 40

        # Dichiaro due liste in cui si inseriscono le gare che hanno l'indice di similarità richiesta
        listVinte = []
        listPerse = []

        certificazioniRichieste = garaAppalto.getCertificazioniRichieste()

        # Si calcolano le certificazioni minime richieste
        certificazioniMinime = math.floor((len(certificazioniRichieste.keys()) / 100) * indiceSimilarita)

        # Per ogni gara d'appalto si calcolano quante gare certificazioni sono in comune
        for each in gareAppaltoVinte:
            stessiValori = 0
            for key in certificazioniRichieste.keys() & each.getCertificazioniRichieste().keys():
                stessiValori += 1

            # E se le certificazioni sono maggiori o uguali di quelle minime richieste, si aggiunge la gara
            # fra quelle vinte
            if(stessiValori >= certificazioniMinime):
                listVinte.append(each)


        print("Gare appalto vinte fra le simili " + str(len(listVinte)))

        for each in gareAppaltoPerse:
            stessiValori = 0
            for key in certificazioniRichieste.keys() & each.getCertificazioniRichieste().keys():
                stessiValori += 1

            if (stessiValori >= certificazioniMinime):
                listPerse.append(each)

        print("Gare appalto perse fra le simili " + str(len(listPerse)))


        # Nel caso in cui la lunghezza delle liste risulta essere diversa, si controlla quale lista contiene più valori
        # cosicchè, colei che ha più gare, sancisce il risulto finale
        if len(listVinte) != len(listPerse):
            if len(listVinte) > len(listPerse):
                print("Potresti vincere la gara")
            elif len(listPerse) > len(listVinte):
                print("Potresti perdere la gara")


        # Nel caso in cui invece ci sono lo stesso numero di gare, si calcola la distanza euclidea fra gli
        # importi dell gare.
        elif len(listVinte) == len(listPerse) and len(listVinte) + len(listPerse) != 0:

            distanzeGareVinte = []

            # Fra tutte le gare con il numero minimo delle caratteristiche richieste, si fa una sommatoria delle
            # differenze fra i valori, e si calcola la radice quadrata di quest'ultima
            for each in listVinte:
                sommatoria = 0
                for key in certificazioniRichieste.keys() & each.getCertificazioniRichieste().keys():
                    sommatoria = sommatoria + pow(certificazioniRichieste[key]["ImportoClass"] -
                                         each.getCertificazioniRichieste()[key]["ImportoClass"],2)

                distanzeGareVinte.append(math.sqrt(sommatoria))


            distanzeGarePerse = []
            for each in listPerse:
                sommatoria = 0
                for key in certificazioniRichieste.keys() & each.getCertificazioniRichieste().keys():
                    sommatoria = sommatoria + pow(certificazioniRichieste[key]["ImportoClass"] -
                                                  each.getCertificazioniRichieste()[key]["ImportoClass"], 2)


                distanzeGarePerse.append(math.sqrt(sommatoria))

            # Ed infine se la distanza massima appartiene alle gare perse, allora è probabile che si possa
            # vincere la gara, o viceversa.
            if max(distanzeGarePerse) > max(distanzeGareVinte):
                print("Potresti vincere la gara")
            else:
                print("Potresti perdere la gara")

        # Nel caso non ci siano abbastanza certificazioni.
        else:
            print("Non ci sono " + str(certificazioniMinime) + " certificazioni in comune con questa gara")







