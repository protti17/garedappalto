
import pandas as pd

class GestoreCertificazioni:

    @staticmethod
    def checkCertificazione(tipo):

        # Attraverso l'utilizzo della libreria sopra descritta, leggo il file csv, la potenzialità di questo metodo
        # sta nel fatto che, indicando nel primo rigo del file csv che si vuole aprire, il nome della colonna,
        # essa risulta valere anche come indice per la ricerca dei valori, infatti, da come si può notare, il termine
        # "Classificazione" risulta essere la prima colonna all'interno del file CSV.

        df = pd.read_csv("..\categorie.csv", delimiter=',')
        for x in range(0, len(df["Classificazione"])):
            if tipo == df["Classificazione"][x]:
                return x
                break
        return -1

        # Si ritorna la descrizione delle varie classificazioni


    @staticmethod
    def getDescrizione(tipo):
            df = pd.read_csv("..\categorie.csv", delimiter=',')
            return df["Specializzazione"][GestoreCertificazioni.checkCertificazione(tipo)]

    @staticmethod
    def getObbligatorieta(tipo):
        df = pd.read_csv("..\categorie.csv", delimiter=',')
        for x in range(0, len(df["Classificazione"])):
            if tipo == df["Classificazione"][x]:
                if df["QualificazioneObbligatoria"][x] == "SI":
                    return 1
                else:
                    return 0